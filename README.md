# grafana-cn

#### 介绍

Grafana 7.2.1 汉化版，基于 Github 上面别人开源的源码打包，开箱即用：https://github.com/chenweil/grafana/tree/grafana-chs-7.2.1

Grafana Pie Plugin 1.6.1 插件汉化版，基于 Github 上面别人开源的源码打包，开箱即用：https://github.com/chenweil/piechart-panel


#### Grafana 汉化版安装教程

1.  下载相同版本的 Grafana 官方 tar.gz 包解压
2.  使用该项目中的 bin 和 public 替换掉官方包中的对应目录
3.  启动 Grafana 即可

#### Grafana Pie Plugin 汉化版安装教程

1.  将插件移动到 Grafana 插件目录会自动安装
